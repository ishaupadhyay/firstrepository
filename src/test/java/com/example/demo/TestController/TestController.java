package com.example.demo.TestController;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;


import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.hamcrest.Matchers.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.demo.Cotroller.Controller1;
import com.example.demo.Cotroller.ControllerRest;
import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;
import com.example.demo.Service.EmpService1;
import static org.hamcrest.Matchers.*;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
@WebMvcTest(ControllerRest.class)



public class TestController {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private EmployeeRepository emprepo;
	@MockBean
	private EmpService1 empSer;
     @Test
     public void testGetEmployees() throws Exception
     {
    	 Employee e=new Employee();
    	 e.setId(1);
 		e.setFirstname("Priya");
 		e.setSurname("Rajendran");
 		e.setSalary(50000);
 		e.setGender("F");
 		Employee e2=new Employee();
 		e2.setId(2);
 		e2.setFirstname("Nikki");
 		e2.setSurname("Mal");
 		e2.setSalary(22);
 		e2.setGender("F");
 		List<Employee> elist=new ArrayList<>();
 		elist.add(e);
 		elist.add(e2);
 		
 		Mockito.when(empSer.listAll()).thenReturn(elist);
 		
		String URI = "/empser";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expectedJson = this.mapToJson(elist);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
		System.out.print(outputInJson);
     }
     @Test
     public void updateEmployee() throws Exception
     {
    	 Employee e=new Employee();
    	 int id=1;
    	 e.setId(1);
 		e.setFirstname("Priya");
 		e.setSurname("Rajendran");;
 		e.setSalary(50000);
 		e.setGender("F");
 		Optional<Employee> e1=Optional.of(e);
 		Mockito.when(emprepo.findById(id)).thenReturn(e1);
 		
 		Mockito.when(empSer.updateEmployee(id,e)).thenReturn(e1);
 		
 		 String URI="/emp/{id}";
 		String expectedJson = this.mapToJson(e);
		
 		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(
				"/emp/"+id).accept(MediaType.APPLICATION_JSON).content(expectedJson)
				.contentType(MediaType.APPLICATION_JSON);;
				MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		
				MockHttpServletResponse response = result.getResponse();
				String outputInJson = response.getContentAsString();
				
				
				assertEquals(HttpStatus.OK.value(), response.getStatus());
     }
     private String mapToJson(Object object) throws JsonProcessingException {
 		ObjectMapper objectMapper = new ObjectMapper();
 		return objectMapper.writeValueAsString(object);
 	}




}
