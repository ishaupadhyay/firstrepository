package com.example.demo.servicetest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;
import com.example.demo.Service.EmpService1;

public class ServiceTest {
	@Autowired 
	private EmpService1 empSer;
	@MockBean
	private EmployeeRepository emprepo;
	@Test
	public void  createEmployeeTest()
	{
		Employee e=new Employee();
		e.setFirstname("Priya");
		e.setSurname("Rajendran");
		e.setSalary(50000);
		e.setGender("F");
		Mockito.when(emprepo.save(e)).thenReturn(e);
		assertThat(empSer.addEmployee(e)).isEqualTo(e);
	}
	@Test
	public void  getEmployeeListTest()
	{
		Employee e=new Employee();
		e.setFirstname("Priya");
		e.setSurname("Rajendran");
		e.setSalary(50000);
		e.setGender("F");
		Employee e2=new Employee();
		e2.setFirstname("Nikki");
		e2.setSurname("Mal");
		e2.setSalary(22000);
		e2.setGender("F");
		List<Employee> elist=new ArrayList<>();
		elist.add(e);
		elist.add(e2);
		
		Mockito.when(emprepo.findAll()).thenReturn(elist);
		assertThat(empSer.listAll()).isEqualTo(elist);
		
		
	}
	




}
