package com.example.demo.daotest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;
import static org.assertj.core.api.Assertions.assertThat;
@RunWith(SpringRunner.class)
//@SpringBootTest
@DataJpaTest

public class DAOTest {
	@Autowired
	private EmployeeRepository emprp;
	@Test
	public void saveTest()
	{
		Employee e=new Employee();
		e.setFirstname("Priya");
		e.setSurname("Rajendran");
		e.setSalary(50000);
		e.setGender("F");
		Employee esave=emprp.save(e);
		assertNotNull(esave);
	}  
	@Test
	public void testListProducts() {
	    List<Employee> emp = (List<Employee>) emprp.findAll();
	    assertThat(emp).size().isGreaterThan(0);
	}

}
