package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.demo.Entity.Employee;
@EnableJpaRepositories
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	
  
}

