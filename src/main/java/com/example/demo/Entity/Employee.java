package com.example.demo.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="EMPLOYEE")

public class Employee {
	 @Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String firstname;
	private String surname;
	private int salary;
	private String gender;
	
	 @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Employee() {
		super();
	}
	public Employee(int id, String forename, String surname, int age, String company, String postcode) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.surname = surname;
		this.salary = salary;
		this.gender = gender;
		
	}

	



}
