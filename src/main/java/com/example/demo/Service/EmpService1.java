package com.example.demo.Service;

import java.util.List;
import java.util.Optional;

import com.example.demo.Entity.Employee;

public interface EmpService1 {

	

	List<Employee> listAll();

	Employee addEmployee(Employee emp);

	
	  Employee findId(Integer id );

	Optional<Employee> updateEmployee(Integer id, Employee e);

	Optional<Employee> deleteEmployee(Integer id);


}

